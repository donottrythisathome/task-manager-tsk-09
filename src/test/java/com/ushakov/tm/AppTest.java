package com.ushakov.tm;

import static org.junit.Assert.assertTrue;

import com.ushakov.tm.bootstrap.Bootstrap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Rule
    public ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

    @Test
    public void showVersion() {
        expectedSystemExit.expectSystemExitWithStatus(0);
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run("-v");
    }

    public void showAbout() {
        expectedSystemExit.expectSystemExitWithStatus(0);
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run("-a");
    }

}
