package com.ushakov.tm.repository;

import com.ushakov.tm.api.ICommandRepository;
import com.ushakov.tm.constant.ArgumentConst;
import com.ushakov.tm.constant.TerminalConst;
import com.ushakov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info."
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show application version."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Terminate console application."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show system information."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, EXIT, COMMANDS, ARGUMENTS
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}
